#**Hungry Networks**

Imani Khan

##ENG 1900-05 Short Composition #2


* * *
![](http://cs8.pikabu.ru/post_img/2016/07/28/11/14697343921344242.jpg)

The network or for-profit corporations watch and record your
 every move online. If you are anywhere near civilization, you are on the grid and can be [found, watched, observed, scrutinized.](https://www.youtube.com/watch?v=V9_PjdU3Mpo) People often say that once you put something on the internet, it stays there forever. Steven Shaviro describes this inescapable reality:
 “There is no place of indemnity that would somehow be free of these constraints” (5). 

Using the information they gather, they compile a version of you that may be private and more personal than the ‘you’ you show to the public. In this sense, the network becomes
 more intimate and close to the real you than even many humans in your life. 

With this accumulating reserve of data on you, the network and corporations that control it can then control much of your experience online. They can bombard you with advertisements
 tailored to you, making choices *for* you and what you see. 

Since everything is digital and remotely accessible, it feels as though you are free to make your own choices on the network. However, every ad and popup you see constantly bombarding you has been pre-chosen
 to suit your interests, with or without your consent-- **you are not free at all.** 

* * *
![](https://yesteryearsnews.files.wordpress.com/2012/06/swat-that-fly-4-adams-county-free-press-ia-23-may-1917.jpg)



Shaviro presents the example of a businessman who is constantly
 surrounded by a swarm of notifications.  When notifications-- which be likened to insect messengers-- come calling, you cannot choose to ignore them. Swat or shoo as you may, there will always be more to bother you. Shaviro argues that the meaning of a message
 “cannot be isolated from its mode of propagation, from the way that it harasses me, attacks me, or parasitically invades me” (24). 

Consumers constantly engage with annoying popups and prompts, multitasking on the network. Shaviro summarizes this continuous
 engagement by describing network-users “to be intensely involved, and maximally distracted, all at once” (26).

* * *
![](https://resources.stuff.co.nz/content/dam/images/1/4/6/v/5/5/image.related.StuffLandscapeSixteenByNine.620x349.19a8rl.png/1453233859568.jpg)

With Markdown and any coding in general, you have more direct
 interaction and control as a creator with your content. In contrast to programs that are already coded to a greater extent where you only need to click a button to achieve an aim, Markdown shows you what exactly you are creating and how it works-- essentially,
 the skeleton and meat of your project. 

Though coding will be harder and more time-consuming, Markdown provides a more personal connection in creating, which additionally channels into a greater sense of pride in your work and appreciation for the intricacies
 of the art. 